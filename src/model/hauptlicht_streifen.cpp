/*!
 * \file hauptlicht_streifen.cpp
 * \brief Source für die Repräsentation der Hauptlicht-Streifen
 */
#include "inc/model/hauptlicht_streifen.h"

HauptlichtStreifen::HauptlichtStreifen(uint8_t address)
{
    qDebug() << Q_FUNC_INFO;

    address_ = address;
    last_brightness_ = 0;
    setBrightness(0);
}

void HauptlichtStreifen::saveToFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;
    
    FileHandler* file_handler = new FileHandler(filename);
    file_handler->writeToFile(QString("HauplichtStreifen%1::brightness_").arg(address_), brightness_);
    delete file_handler;
}

void HauptlichtStreifen::loadFromFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;
    int value;
    FileHandler* file_handler = new FileHandler(filename);
    // std::string name = "HauplichtStreifen" + std::to_string(address_) + "::brightness_";
    try {
	    value = file_handler->readFromFile<int>(QString("HauplichtStreifen%1::brightness_").arg(address_));        
	    delete file_handler;
    } 
    catch (...) {
        if (filename == OFF_SETTINGS){
            value = 0;
        }
        else{
            value = 70;
        }
	    delete file_handler;
        saveToFile(filename);
    }
    // make sure to apply new values
    setBrightness(value);
}

void HauptlichtStreifen::setBrightness(int val)
{
    qDebug() << Q_FUNC_INFO;

    brightness_ = val;
    UpdateFunc updater = [this](){hw::writeValue(address_, int(brightness_ * 2.55));};
    qDebug() << "brightness: "<< brightness_ << " last brightness: " << last_brightness_;
    addToUpdaters(updater);
}

int HauptlichtStreifen::getBrightness()
{
    qDebug() << Q_FUNC_INFO;

    return brightness_;
}

bool HauptlichtStreifen::isOn()
{
    // qDebug() << Q_FUNC_INFO;
	return brightness_ != 0;
}

void HauptlichtStreifen::setOn(bool state)
{
    qDebug() << Q_FUNC_INFO;
    
    if (state)
    {	
    	brightness_ = last_brightness_;
        setBrightness(brightness_);
    }
    else
    {	
    	if (not isOn()){
    		// leads to storage of 0 in last_brightness_
    		QString msg = "Hauptlichtstreifen was switched off but was already off.";
    		log(ERROR_LOG, msg);
    	}
    	last_brightness_ = brightness_;
    	setBrightness(0);
    }
}
