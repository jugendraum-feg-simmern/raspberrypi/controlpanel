#include "inc/model/theken_deckenlicht.h"

ThekenDeckenlicht::ThekenDeckenlicht()
	: JElement ()
{
	qDebug() << Q_FUNC_INFO;
	
	setOn(false);
}

void ThekenDeckenlicht::saveToFile(QString filename)
{
	qDebug() << Q_FUNC_INFO;
    FileHandler* file_handler_ = new FileHandler(filename);
    file_handler_->writeToFile("ThekenDeckenlicht::is_on_", is_on_);
    delete file_handler_;
}

void ThekenDeckenlicht::loadFromFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;      
    FileHandler* file_handler_ = new FileHandler(filename);
    try {
    	setOn( file_handler_->readFromFile<bool>("TheckenDeckenlicht::is_on_") );
	    delete file_handler_;
    } 
    catch (...){
    	setOn(false);
        delete file_handler_;
        saveToFile(filename);
    }
    	
}

bool ThekenDeckenlicht::isOn()
{
    // qDebug() << Q_FUNC_INFO;
	return is_on_;
}

void ThekenDeckenlicht::setOn(bool state)
{
    qDebug() << Q_FUNC_INFO;
    is_on_ = state;
   	
    UpdateFunc updater = [this](){hw::writeState(THEKE, is_on_);};
    addToUpdaters(updater);
}


