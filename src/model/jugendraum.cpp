/*!
 * \file jugendraum.cpp
 * \brief Source der Jugendraum Klasse.
 */
#include "inc/model/jugendraum.h"

Jugendraum::Jugendraum()
{
    qDebug() << Q_FUNC_INFO;
    log(EVENT_LOG, "ControlPanel-3-0 started");
    log(ERROR_LOG, "[ INFO ] ControlPanel-3-0 started"); // log to ERROR_LOG to make restarts directly visible in the file

    hw::init();

    update_timer_ = new QTimer();
    connect(update_timer_, SIGNAL(timeout()), this, SLOT(update()));
    update_timer_->start(HW_UPDATE_INTERVAL_MS);

    single_shot_timer_ = new QTimer();
    single_shot_timer_->setSingleShot(true);

    // create all the members
    theken_rgb = new ThekenRGB();
    theken_deckenlicht = new ThekenDeckenlicht();
    paletten_licht = new PalettenLicht();
    wand_rgb = new WandRGB();
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_1));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_2));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_3));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_4));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_5));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_6));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_7));
    hauptlicht.append(new HauptlichtStreifen(HL_STREIFEN_8));
    for (int i = 1; i < 19; i++)
    	rgb_deckenlicht.append(new RGBStreifen(i));
    	
	all_elements_.append(theken_rgb);
	all_elements_.append(theken_deckenlicht);
	all_elements_.append(paletten_licht);
	all_elements_.append(wand_rgb);
	for (auto streifen: hauptlicht)
		all_elements_.append(streifen);
		
	for (auto rgb_streifen: rgb_deckenlicht)
   		all_elements_.append(rgb_streifen);   
}

Jugendraum::~Jugendraum()
{
    update_timer_->stop();
    delete update_timer_;

    qDebug() << Q_FUNC_INFO;
    delete paletten_licht;
    delete theken_rgb;
    delete theken_deckenlicht;
    delete wand_rgb;
    
    for (auto streifen: hauptlicht)
        delete streifen;
        
    for (auto rgb_streifen: rgb_deckenlicht)
        delete rgb_streifen;

    log(EVENT_LOG, "ControlPanel-3-0 closed");
    log(ERROR_LOG, "[ INFO ] ControlPanel-3-0 closed");
}

void Jugendraum::update()
{
    if (hw::switchEventPending())
    {
        jugendraum_is_on_ = !jugendraum_is_on_;
        loadAllFromFile((jugendraum_is_on_) ? DEFAULT_SETTINGS : OFF_SETTINGS);
        emit updated();
        hw::switchEventProcessed();
    }
	
	for (auto elem: all_elements_)
		elem->update();

    checkJugendraumOn();
    checkHauptlichtOn();
}

void Jugendraum::saveAllToFile(QString filename)
{
    log(EVENT_LOG, QString("Saved settings to %1").arg(filename));

	for (auto elem: all_elements_)
		elem->saveToFile(filename);
}   

void Jugendraum::loadAllFromFile(QString filename)
{
	for (auto elem: all_elements_)
		elem->loadFromFile(filename);
}

void Jugendraum::checkHauptlichtOn()
{
    bool tmp = false;

    for (HauptlichtStreifen *s : hauptlicht)
        tmp |= s->isOn();

    // only act when changed
    if (tmp && !hauptlicht_on_)
    {
        hauptlicht_on_ = tmp;
        hw::writeState(NETZTEIL48V, hauptlicht_on_);
        single_shot_timer_->stop();
    }
    else if(!tmp && hauptlicht_on_)
    {
        hauptlicht_on_ = tmp; // should this be set here or in setHauptlichtRelaisOff() ??
        single_shot_timer_->singleShot(5000, this, SLOT(setHauptlichtRelaisOff()));
    }
}

void Jugendraum::checkJugendraumOn()
{
    bool tmp = false;

    for (auto elem: all_elements_)
        tmp |= elem->isOn();

    jugendraum_is_on_ = tmp;
}

void Jugendraum::setHauptlichtRelaisOff()
{
    qDebug() << Q_FUNC_INFO;

    hauptlicht_on_ = false;
    hw::writeState(NETZTEIL48V, false);
}
