/*!
 * \file rgb_element.cpp
 * \brief Source für die RGB-Basisklasse
 */

#include "inc/model/rgb_element.h"

RGBElement::RGBElement() :
    JElement ()
{
    
}

RGBElement::~RGBElement()
{

}

int RGBElement::getRedValue()
{
    qDebug() << Q_FUNC_INFO;
    return red_value_;
}

int RGBElement::getGreenValue()
{
    qDebug() << Q_FUNC_INFO;
    return green_value_;
}

int RGBElement::getBlueValue()
{
    qDebug() << Q_FUNC_INFO;
    return blue_value_;
}

void RGBElement::setRedValue(int value)
{
    qDebug() << Q_FUNC_INFO;
    red_value_ = value;
    
    // add update function to list if it isn't already in there
    UpdateFunc updater = [this](){hw::writeValue(addr_red_, red_value_);};
    addToUpdaters(updater);
}

void RGBElement::setGreenValue(int value)
{
    qDebug() << Q_FUNC_INFO;
    green_value_ = value;
    
    // add update function to list if it isn't already in there
    UpdateFunc updater = [this](){hw::writeValue(addr_green_, green_value_);};
    addToUpdaters(updater);
}

void RGBElement::setBlueValue(int value)
{
    qDebug() << Q_FUNC_INFO;
    blue_value_ = value;
    
    // add update function to list if it isn't already in there
    UpdateFunc updater = [this](){hw::writeValue(addr_blue_, blue_value_);};
    addToUpdaters(updater);
}

bool RGBElement::isOn()
{
    // qDebug() << Q_FUNC_INFO;
	return (red_value_ + green_value_ + blue_value_) != 0;
}

void RGBElement::setOn(bool state)
{
    qDebug() << Q_FUNC_INFO;
    
    if (state)
    {
        // write the last values to the hardware
        red_value_ = last_red_value_;
        green_value_ = last_green_value_;
        blue_value_ = last_blue_value_;
        setRedValue(red_value_);
        setGreenValue(green_value_);
        setBlueValue(blue_value_);
    }
    else
    {
    	if (not isOn()){
    		// leads to storage of 0 in last_brightness_
    		QString msg = "RGBElement was switched off but was already off.";
    		log(ERROR_LOG, msg);
    	}
        // sets all values to 0 . The old values are stored for later usage
        last_red_value_ = red_value_;
        last_green_value_ = green_value_;
        last_blue_value_ = blue_value_;
        setRedValue(0);
        setGreenValue(0);
        setBlueValue(0);
    }
    
}

