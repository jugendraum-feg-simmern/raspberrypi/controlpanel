/*!
 * \file wand_rgb.cpp
 * \brief Source-File für die Wand-RGB-Streifen
 */
#include "inc/model/wand_rgb.h"

WandRGB::WandRGB():
    RGBElement()
{
    qDebug() << Q_FUNC_INFO;
    
    addr_red_ = RGB_WAND_ROT;
    addr_green_ = RGB_WAND_GRUEN;
    addr_blue_ = RGB_WAND_BLAU;

    setRedValue(0);
    setGreenValue(0);
    setBlueValue(0);
    setOn(false);
}
void WandRGB::saveToFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;
    
    FileHandler* file_handler_ = new FileHandler(filename);
    file_handler_->writeToFile("WandRGB::red_value_", red_value_);
    file_handler_->writeToFile("WandRGB::green_value_", green_value_);
    file_handler_->writeToFile("WandRGB::blue_value_", blue_value_);
    file_handler_->writeToFile("WandRGB::is_on_", isOn());
    delete file_handler_;
}

void WandRGB::loadFromFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;
    
    FileHandler* file_handler_ = new FileHandler(filename);
    int r = 0;
    int g = 0;
    int b = 0;
    try {
	    red_value_ = file_handler_->readFromFile<int>("WandRGB::red_value_");
	    green_value_ = file_handler_->readFromFile<int>("WandRGB::green_value_");
	    blue_value_ = file_handler_->readFromFile<int>("WandRGB::blue_value_");
	    delete file_handler_;
    }
    catch (...) {
        delete file_handler_;
        // save zeros to variables that were not overwritten because of error
        saveToFile(filename);
    }
    // make sure to apply loaded values
    setRedValue(r);
    setGreenValue(g);
    setBlueValue(b);
}
