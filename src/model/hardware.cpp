#include "inc/model/hardware.h"

namespace hw
{
    // something like a hack for privacy in namespaces
    namespace
    {
        // file descriptor
        int mcp23017;
        int pca9635;
        // store slider registers
        QList<QByteArray> slider_setpoints, slider_analogs, slider_actives;
        // udp
        QUdpSocket* udp_slider_ = new QUdpSocket();
        QUdpSocket* udp_temperatur_ = new QUdpSocket();

        /*!
         * \brief sliderless_ used temporarily to disable slider features if they are not hooked up
         * ny slider-related function will return 0
         */
        bool sliderless_ = false;
        bool sensorless_ = false;

        bool switch_event_pending_ = false;
        qint64 last_isr_timestamp_ = 0;
        int switch_debounce_time_ms = 200;

        void switchToggled(){
            if (QDateTime::currentMSecsSinceEpoch() - last_isr_timestamp_ > switch_debounce_time_ms)
            {
                last_isr_timestamp_ = QDateTime::currentMSecsSinceEpoch();
                if (readGPIO(LIGHT_SWICH_A) == readGPIO(LIGHT_SWICH_B))
                    switch_event_pending_ = true;
                else
                    log(ERROR_LOG, "[ ISR ] switch pins not equal");
            }
            else
            {
                qDebug() << "hüpf " << QDateTime::currentMSecsSinceEpoch() - last_isr_timestamp_;
            }
        }
    }
}

void hw::init()
{
    qDebug() << Q_FUNC_INFO;

    // Setup I2C
    // setup the portexpander
    mcp23017 = wiringPiI2CSetup(PORTEXPANDER);
    wiringPiI2CWriteReg8(mcp23017, MCP23017_IODIRA, 0x00);
    wiringPiI2CWriteReg8(mcp23017, MCP23017_IODIRB, 0x0f);
    // init the output registers
    wiringPiI2CWriteReg8(mcp23017, MCP23017_GPIOA, 0x00);
    wiringPiI2CWriteReg8(mcp23017, MCP23017_GPIOB, 0x00);

    // setup the PCA
    pca9635 = wiringPiI2CSetup(PCA_LICHT);
    wiringPiI2CWriteReg8(pca9635, PCA9635_MODE1, 0x00);     // disable ALLCALL_ADDR
    wiringPiI2CWriteReg8(pca9635, PCA9635_MODE2, 0x1d);
    wiringPiI2CWriteReg8(pca9635, PCA9635_GRPPWM, 0x00);
    wiringPiI2CWriteReg8(pca9635, PCA9635_LEDOUT0, 0xaa);
    wiringPiI2CWriteReg8(pca9635, PCA9635_LEDOUT1, 0xaa);
    wiringPiI2CWriteReg8(pca9635, PCA9635_LEDOUT2, 0xaa);
    wiringPiI2CWriteReg8(pca9635, PCA9635_LEDOUT3, 0xaa);
    // setup the output registers
    for (int i = 2; i < 10; i++)
        wiringPiI2CWriteReg8(pca9635, i, 0x00);

    // bind udp_slider_ to the adress of the arduino
    udp_slider_->bind(SLIDER_UDP);
    // and get the addresses
    udp_slider_->writeDatagram(QByteArray("x"), SLIDER_UDP);
    if (udp_slider_->waitForReadyRead(1000))
    {
        QByteArray raw = udp_slider_->receiveDatagram().data();
        qDebug() << "raw: " << raw;
        for (int i = 0; i <  4; i++) { slider_setpoints.append(raw.mid(2*i, 2)); }
        for (int i = 4; i <  8; i++) { slider_analogs.append(raw.mid(2*i, 2)); }
        for (int i = 8; i < 12; i++) { slider_actives.append(raw.mid(2*i, 2)); }
        qDebug() << "setpoints: " << slider_setpoints;
        qDebug() << "analogs:   " << slider_analogs;
        qDebug() << "actives:   " << slider_actives;
    } else {
        qDebug() << "Failed to load Arduino's registers!";
        log(ERROR_LOG, "[ UDP ] timeout (1000 ms) reached when loading slider registers");
        for (int i = 0; i <  4; i++) { slider_setpoints.append(nullptr); }
        for (int i = 4; i <  8; i++) { slider_analogs.append(nullptr); }
        for (int i = 8; i < 12; i++) { slider_actives.append(nullptr); }

        sliderless_ = true;
    }

    // bind udp port for temperatur service to a port
    udp_temperatur_->bind(QHostAddress::LocalHost, TEMPERATUR_SERVICE_PORT);
    if (readUDP(QHostAddress::LocalHost, TEMPERATUR_SERVICE_PORT).isNull())
    {
        sensorless_ = true;
    }

    // set up wirinPi to use gpio
    if (wiringPiSetup() == -1){
        log(ERROR_LOG, "[ GPIO ] setup failed");
    }

    // setup ISR for interrupt of light switch
    void (*isr)(void) = &switchToggled;
    wiringPiISR(LIGHT_SWICH_A, INT_EDGE_BOTH, isr);
    wiringPiISR(LIGHT_SWICH_B, INT_EDGE_BOTH, isr);
}

bool hw::readState(uint8_t bank, uint8_t bit)
{
    qDebug() << Q_FUNC_INFO;

    return bit & wiringPiI2CReadReg8(mcp23017, bank);
}

void hw::writeState(uint8_t bank, uint8_t bit, bool state )
{
    qDebug() << Q_FUNC_INFO << " bit: " << bit << ", state: " << state;

    // This is not nice, but somewhat functional
    // There is an issue to improve on this ;)
    uint8_t val_old = wiringPiI2CReadReg8(mcp23017, bank);
    int loop_counter = 0;
    while ((val_old >= 32) && (loop_counter <= 5))
    {
        log(ERROR_LOG, QString("[ MCP23017 ] read value too large, got %1").arg(val_old));
        val_old = wiringPiI2CReadReg8(mcp23017, bank);
        loop_counter ++;
    }
    if (val_old < 32)
    {
        uint8_t val_new = (val_old & ~bit) | ((state) ? bit : 0);
        qDebug() << "old: " << val_old << " -> new: " << val_new;
        wiringPiI2CWriteReg8(mcp23017, bank, val_new);
    }
}

int hw::readValue(uint8_t reg)
{
    qDebug() << Q_FUNC_INFO;

    return wiringPiI2CReadReg8(pca9635, reg);
}

void hw::writeValue(uint8_t reg, int val)
{
    qDebug() << Q_FUNC_INFO;

    wiringPiI2CWriteReg8(pca9635, reg, val);
}

QList<QByteArray> hw::getSliderConfig(int slider_num)
{
    qDebug() << Q_FUNC_INFO;

    QList<QByteArray> tmp;
    tmp.append(slider_setpoints.at(slider_num - 1));
    tmp.append(slider_analogs.at(slider_num - 1));
    tmp.append(slider_actives.at(slider_num - 1));
    return tmp;
}

void hw::writeUDP(QByteArray data, QHostAddress ip, quint16 port)
{
    qDebug() << Q_FUNC_INFO;

    /*
     * use the right socket for the transaction
     * this is done to ensure that the data received was emitted by the recipient
     * of the datagram
     */
    QUdpSocket* udp;
    if ((ip == QHostAddress(SLIDER_IP)) && (port == SLIDER_PORT)) udp = udp_slider_;
    else if ((ip == QHostAddress::LocalHost) && (port == TEMPERATUR_SERVICE_PORT)) udp = udp_temperatur_;
    else udp = new QUdpSocket();

    // transaction
    udp->writeDatagram(data, ip, port);
    if (udp->waitForReadyRead(1000))
    {
        int aw = udp->receiveDatagram().data().toInt();
        if (aw != 0)
        {
            // do something because the write has failed!
            qDebug() << "Client returned " << aw;
            log(ERROR_LOG, QString("[ UDP ] write to %1:%2 failed : client returned %3").arg(ip.toString()).arg(port).arg(aw));
        }
    }
    else
    {
        qDebug() << "Client timeout on write";
        log(ERROR_LOG, QString("[ UDP ] client %1:%2 timeout (1000 ms)").arg(ip.toString()).arg(port));
    }
}

int hw::readUDP(QByteArray reg, QHostAddress ip, quint16 port)
{
    // qDebug() << "want " << reg << "from " << ip << ":" << port;

    /*
     * use the right socket for the transaction
     * this is done to ensure that the data received was emitted by the recipient
     * of the datagram
     */
    QUdpSocket* udp;
    if ((ip == QHostAddress(SLIDER_IP)) && (port == SLIDER_PORT)) udp = udp_slider_;
    else if ((ip == QHostAddress::LocalHost) && (port == TEMPERATUR_SERVICE_PORT)) udp = udp_temperatur_;
    else udp = new QUdpSocket();

    udp->writeDatagram(reg, ip, port);

    int tmp = -1;
    if (udp->waitForReadyRead(1000))
    {
        QNetworkDatagram d = udp->receiveDatagram(512);
        // qDebug() << "sender = " << d.senderAddress() << ":" << d.senderPort() << ", data: " << d.data();
        QByteArray raw = d.data();
        if (raw.length() == 2)
        {
            tmp = (uint8_t(raw.at(0)) << 8) + uint8_t(raw.at(1));
        }
        else
        {
            qDebug() << "data not as exspected, got " << raw;
            log(ERROR_LOG, QString("[ UDP ] corrupted data from %1:%2 : expected 2 bytes, got %3").arg(ip.toString(), port, raw.length()));
        }
    }
    else
    {
        qDebug() << "Failed to load Arduino's registers!";
        log(ERROR_LOG, QString("[ UDP ] client %1:%2 timeout (1000 ms)").arg(ip.toString(), port));
    }
    return tmp;
}

QJsonDocument hw::readUDP(QHostAddress ip, quint16 port)
{
    qDebug() << Q_FUNC_INFO;

    QUdpSocket* udp;
    if ((ip == QHostAddress::LocalHost) && (port == TEMPERATUR_SERVICE_PORT)) udp = udp_temperatur_;
    else udp = new QUdpSocket();

    // send anything to trigger transaction
    udp->writeDatagram(QByteArray("x"), ip, port);

    // read the answer
    QJsonDocument jo;
    if (udp->waitForReadyRead(1000))
    {
        QNetworkDatagram d = udp->receiveDatagram();
        jo = QJsonDocument::fromJson(d.data());
        if (jo.isNull())
        {
            log(ERROR_LOG, QString("[ UDP ] failed to convert data from %1:%2 to json").arg(ip.toString()).arg(port));
        }
    }
    else
    {
        qDebug() << "Failed to read temperatures!";
        log(ERROR_LOG, QString("[ UDP ] client %1:%2 timeout (1000 ms)").arg(ip.toString()).arg(port));
    }
    return jo;
}

bool hw::sliderless()
{
    qDebug() << Q_FUNC_INFO;

    return sliderless_;
}

bool hw::sensorless()
{
    qDebug() << Q_FUNC_INFO;

    return sensorless_;
}

void hw::writeGPIO(const int pin, const bool state)
{
    qDebug() << Q_FUNC_INFO;

    pinMode(pin, OUTPUT);
    digitalWrite(pin, (state)? 1: 0);
}

bool hw::readGPIO(const int pin)
{
    qDebug() << Q_FUNC_INFO;

    pinMode(pin, INPUT);
    int val = digitalRead(pin);
    return (val==1)? true : false;
}

bool hw::switchEventPending()
{
    return switch_event_pending_;
}

void hw::switchEventProcessed()
{
    qDebug() << Q_FUNC_INFO;

    switch_event_pending_ = false;
}
