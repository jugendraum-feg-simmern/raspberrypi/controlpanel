/*!
 * \file rgb_streifen.cpp
 * \brief Source der Repräsentation der RGB-Decken-Streifen
 */
#include "inc/model/rgb_streifen.h"

RGBStreifen::RGBStreifen(uint8_t address) :
    RGBElement ()
{
    address_ = address;

    setRedValue(0);
    setGreenValue(0);
    setBlueValue(0);
    setOn(false);
}

void RGBStreifen::saveToFile(QString filename){
    FileHandler* file_handler = new FileHandler(filename);
    file_handler->writeToFile(QString("RGBStreifen%1::red_value_").arg(address_), red_value_);
    file_handler->writeToFile(QString("RGBStreifen%1::green_value_").arg(address_), green_value_);
    file_handler->writeToFile(QString("RGBStreifen%1::blue_value_").arg(address_), blue_value_);
    delete file_handler;
}

void RGBStreifen::loadFromFile(QString filename){
    FileHandler* file_handler = new FileHandler(filename);
    int r = 0;
    int g = 0;
    int b = 0; 
    try {
        r = file_handler->readFromFile<int>(QString("RGBStreifen%1::red_value_").arg(address_));
    	g = file_handler->readFromFile<int>(QString("RGBStreifen%1::green_value_").arg(address_));
	    b = file_handler->readFromFile<int>(QString("RGBStreifen%1::blue_value_").arg(address_));
	    delete file_handler;
    }
    catch (...){
        delete file_handler;
        // save zeros to variables that were not overwritten because of error
        saveToFile(filename);
    }
    // make sure to apply new values
    setRedValue(r);
    setGreenValue(g);
    setBlueValue(b);
}
