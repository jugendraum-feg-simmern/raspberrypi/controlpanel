#include "inc/model/theken_rgb.h"

ThekenRGB::ThekenRGB() :
    RGBElement()
{
    qDebug() << Q_FUNC_INFO;
    
    addr_red_ = RGB_THEKE_ROT;
    addr_green_ = RGB_THEKE_GRUEN;
    addr_blue_ = RGB_THEKE_BLAU;    
        
    setRedValue(0);
    setGreenValue(0);
    setBlueValue(0);
    setOn(false);
}

void ThekenRGB::saveToFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;
    FileHandler* file_handler_ = new FileHandler(filename);
    file_handler_->writeToFile("ThekenRGB::red_value_", red_value_);
    file_handler_->writeToFile("ThekenRGB::green_value_", green_value_);
    file_handler_->writeToFile("ThekenRGB::blue_value_", blue_value_);
    delete file_handler_;
}

void ThekenRGB::loadFromFile(QString filename)
{
    qDebug() << Q_FUNC_INFO;
    FileHandler* file_handler_ = new FileHandler(filename);
    int r = 0;
    int g = 0;
    int b = 0;
    try {
	    red_value_ = file_handler_->readFromFile<int>("ThekenRGB::red_value_");
    	green_value_ = file_handler_->readFromFile<int>("ThekenRGB::green_value_");
	    blue_value_ = file_handler_->readFromFile<int>("ThekenRGB::blue_value_");
    	delete file_handler_;
	}
	catch (...) {
        delete file_handler_;
        // save zeros to variables that were not overwritten because of error
        saveToFile(filename);
	}
	// make sure to apply the new values
	setRedValue(r);
	setGreenValue(g);
	setBlueValue(b);
}