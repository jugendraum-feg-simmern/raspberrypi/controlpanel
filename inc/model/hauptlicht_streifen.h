/*!
 * \file hauptlicht_streifen.h
 * \brief Header für die Repräsentation der Hauptlicht-Streifen
 */
#ifndef HAUPTLICHT_STREIFEN_H
#define HAUPTLICHT_STREIFEN_H

#include <QDebug>
#include <QString>
// #include <string>

#include "inc/model/j_element.h"
#include "inc/model/hardware.h"

class HauptlichtStreifen : public JElement
{
public:
    /*!
     * \brief HauptlichtStreifen
     * \param address Register des Streifens; verwende Macro aus hardware_config.h
     * \todo Wenn SetOn() mit true aufgerufen wird und der interne Wert 0 ist, einen Default-Wert aus 
     * einer Datei lesen
     */
    HauptlichtStreifen(uint8_t address);
    
    void saveToFile(QString filename) override;
    
    /*!
     * \brief Lädt die Einstellungen aus der Datei \a filename. 
     * Wird der Wert in der Datei nicht gefunden oder bei anderen Fehlern wird 
     * als Wert 0 im Falle der off_settings oder 70 sonst geladen und direkt in die Datei geschrieben
     * \param filename Name der Datei, aus der gelesen wird.
     */
    void loadFromFile(QString filename) override;

    /*!
     * \brief Setzt den Helligkeitswert, wenn der Streifen aktiviert ist
     * \param val Helligkeitswert zwischen 0 und 100
     */
    void setBrightness(int val);

    /*!
     * \brief getValue
     * \return Helligkeitswert zwischen 0 und 100
     */
    int getBrightness();
    
    /*!
     * \brief Bestimmt den Zustand (An oder Aus) des Streifens aus dem Helligkeitswert
     * \return true wenn an
     */
    bool isOn() override;
    
    /*!
     * \brief Schaltet den Streifen an und aus, wenn der Streifen aktiviert ist
     * \param state true = an
     */
    void setOn(bool state) override;

private:
    int brightness_;        //!< aktueller Helligkeitswert
    int last_brightness_;	//!< letzter Helligkeitswert
    uint8_t address_;       //!< Register des Streifens, verwende Macro aus hardware_config.h
};

#endif // HAUPTLICHT_STREIFEN_H
