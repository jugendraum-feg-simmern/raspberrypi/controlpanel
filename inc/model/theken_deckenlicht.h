#ifndef THEKEN_DECKENLICHT_H
#define THEKEN_DECKENLICHT_H

#include "inc/model/j_element.h"
#include "inc/model/hardware.h"

class ThekenDeckenlicht : public JElement
{
public:
	//! Constructor
	ThekenDeckenlicht();
	
	void saveToFile(QString filename) override;
	void loadFromFile(QString filename) override;

    /*!
     * \brief Gibt den An-Aus-Zustand zurück
     * \return true ist an
     */
	bool isOn() override;	
	
    /*!
     * \brief schaltet an oder aus
     * \param state true = an
     */
	void setOn(bool state) override;
	
private:
	//! speichert den An-Aus-Zustand
	bool is_on_;
};



#endif // THEKEN_DECKENLICHT_H
